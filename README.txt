DESCRIPTION
===========
PayU Romania (http://www.payu.ro) integration for the Drupal Commerce payment and checkout system.
This module is forked from Commerce ePayment written by Andrei Mateescu (https://drupal.org/user/729614)

INSTALLATION INSTRUCTIONS FOR COMMERCE PAYU ROMANIA
===================================================

First you should make sure you have an PayU Romania Merchant account, and are ready to configure it:

Installing & configuring the PayU Romania payment method in Drupal Commerce
---------------------------------------------------------------------------
- Enable the module (Go to admin/modules and search in the Commerce (Contrib) fieldset).
- Go to Administration > Store > Configuration > Payment Methods
- Under "Disabled payment method rules", find the PayU Romania payment method
- Click the 'enable' link
- Once "PayU Romania" appears in the "Enabled payment method rules", click on it's name to configure
- In the table "Actions", find "Enable payment method: PayU Romania" and click the link
- Under "Payment settings", you can configure the module:
  * Instant Payment Notification (IPN) URL: use this URL to configure the IPN settings in the PayU Romania Control Panel (more info below)
  * PayU Romania account: define whether to use the test parameter
  * PayU Romania Merchant ID: add your PayU Romania Vendor code
  * PayU Romania Key: add your PayU Romania Key coding
  * Currency code: select a currency
  * Language code: let PayU Romania know in which language the payment screens should be presented
  * IPN logging: define wheter to log full IPN notifications (used for debugging)


Configuring PayU Romania
------------------------
- Log in with your PayU Romania merchant account
- Go to Account management > Account settings > IPN settings
- Enter the URL copied from the "Payment settings" above (it should be something like: http://www.example.com/commerce_payu_romania/ipn)
- You can now process payments with PayU Romania!


Credits
=======
Madalin Ignisca (https://drupal.org/user/718388) @ Colourweb.ro
Andrei Mateescu (https://drupal.org/user/729614) initial developer of the ePayment module (ePayment was PayU Romania before)
